package TZConverterTZToUTC;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

public class FixedValue {
	public static void main(String... args) {
		String desiredSourceTZ = "+07:00";		
		
        DateTimeZone tzSource = DateTimeZone.forID(desiredSourceTZ);
        DateTimeZone tzTarget = DateTimeZone.UTC;

        DateTime nowSource = DateTime.now(tzSource);
        DateTime nowTarget = nowSource.withZone(tzTarget);
        
		System.out.println("nowSource:");
        System.out.println(nowSource);
        System.out.println("\nnowTarget:");
		System.out.println(nowTarget);
    }	
}