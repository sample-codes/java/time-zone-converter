package TZConverterTZToUTC;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

public class SystemTZ {
	public static void main(String... args) {
		// current moment in system time zone
		DateTime nowSource = new DateTime();
		DateTimeZone tzTarget = DateTimeZone.UTC;

        
        DateTime nowTarget = nowSource.withZone(tzTarget);
        
		System.out.println("nowSource:");
        System.out.println(nowSource);
        System.out.println("\nnowTarget:");
		System.out.println(nowTarget);
    }	
}
