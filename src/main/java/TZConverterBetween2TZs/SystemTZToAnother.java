package TZConverterBetween2TZs;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

public class SystemTZToAnother {
	public static void main(String... args) {
		// current moment in system time zone
		DateTime nowSource = new DateTime();
		String desiredTargetTZ = "-05:00";		
		
        DateTimeZone tzTarget = DateTimeZone.forID(desiredTargetTZ);
        
        DateTime nowTarget = nowSource.withZone(tzTarget);
        
		System.out.println("nowSource:");
        System.out.println(nowSource);
        System.out.println("\nnowTarget:");
		System.out.println(nowTarget);
    }	
}
