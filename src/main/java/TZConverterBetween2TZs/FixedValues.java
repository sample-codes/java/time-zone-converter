package TZConverterBetween2TZs;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

public class FixedValues {
	public static void main(String... args) {
		String desiredSourceTZ = "+07:00";
		String desiredTargetTZ = "-03:00";		
		
        DateTimeZone tzSource = DateTimeZone.forID(desiredSourceTZ);
        DateTimeZone tzTarget = DateTimeZone.forID(desiredTargetTZ);

        DateTime nowSource = DateTime.now(tzSource);
        DateTime nowTarget = nowSource.withZone(tzTarget);
        
		System.out.println("nowSource:");
        System.out.println(nowSource);
        System.out.println("\nnowTarget:");
		System.out.println(nowTarget);
    }	
}