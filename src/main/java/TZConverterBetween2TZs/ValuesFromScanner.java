package TZConverterBetween2TZs;
import java.util.Scanner;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

public class ValuesFromScanner {
	public static void main(String... args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("=========\nDesired SOURCE time zone\n=========");
		String desiredSourceTZ = readTZ(scan);
		System.out.println("=========\nDesired TARGET time zone\n=========");
		String desiredTargetTZ = readTZ(scan);
		scan.close();
		System.out.println("============ Result =============");
        DateTimeZone tzSource = DateTimeZone.forID(desiredSourceTZ);
        DateTimeZone tzTarget = DateTimeZone.forID(desiredTargetTZ);

        DateTime nowSource = DateTime.now(tzSource);
        DateTime nowTarget = nowSource.withZone(tzTarget);
        
		System.out.println("nowSource:");
        System.out.println(nowSource);
        System.out.println("\nnowTarget:");
		System.out.println(nowTarget);
    }
	public static String readTZ(Scanner scan) {
		boolean ok = false;
		String sign = "";
		while (!ok) {
			System.out.println("Please, type the desired time zone\nSIGN offset from UTC (options: + or -). If UTC, type +:");			
			sign = scan.next();
			ok = sign.equals("+")||sign.equals("-") ? true : false;
		}
		scan.reset();
		System.out.println("Sign ok!\n-------");

		int hour = 0;
		ok = false;
		while (!ok) {
			System.out.println("Please, type the desired time zone\nHOUR offset from UTC (e.g.: 0...14). If UTC, type 0:");
			hour = scan.nextInt();
			ok = hour>=0 && hour<=14 ? true : false;
		}
		scan.reset();
		System.out.println("Hours ok!\n-------");
		String hourStr = Integer.toString(hour);
		hourStr = hourStr.length() == 1 ? "0"+hourStr : hourStr; 
		
		int minute = 0;
		ok = false;
		while (!ok) {
			System.out.println("Please, type the desired time zone\nMINUTE offset from UTC (e.g.: 0...59). If UTC, type 0:");
			minute= scan.nextInt();
			ok = minute>=0 && minute<=59 ? true : false;
		}
		scan.reset();
		System.out.println("Minutes ok!");
		String minuteStr = Integer.toString(minute);
		minuteStr = minuteStr.length() == 1 ? "0"+minuteStr : minuteStr;
		
		return sign+hourStr+":"+minuteStr;
	}
}